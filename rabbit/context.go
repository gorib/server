package rabbit

import (
	"context"

	amqp "github.com/rabbitmq/amqp091-go"

	"gitlab.com/gorib/session"
)

func NewContextWithMessage(message *amqp.Delivery) context.Context {
	s := session.Session{
		Uri:           message.RoutingKey,
		Method:        "AMQP",
		Body:          string(message.Body),
		CorrelationId: message.CorrelationId,
	}
	if userAgent, ok := message.Headers[session.AmqpUserAgentHeader].(string); ok {
		s.UserAgent = userAgent
	}
	if userLanguage, ok := message.Headers[session.AmqpUserLanguageHeader].(string); ok {
		s.UserLanguage = userLanguage
	}
	if sourceIp, ok := message.Headers[session.AmqpSourceIpHeader].(string); ok {
		s.SourceIp = sourceIp
	}
	return session.NewContextWithSession(context.Background(), s)
}
