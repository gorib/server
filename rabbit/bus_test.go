package rabbit

import (
	"context"
	"errors"
	"testing"
	"time"

	amqp "github.com/rabbitmq/amqp091-go"
	"github.com/stretchr/testify/assert"

	"gitlab.com/gorib/pry"
	"gitlab.com/gorib/session"
)

var (
	ackHandler    func(tag uint64, multiple bool) error
	nackHandler   func(tag uint64, multiple bool, requeue bool) error
	rejectHandler func(tag uint64, requeue bool) error
)

type acknowledger struct {
}

func (a *acknowledger) Ack(tag uint64, multiple bool) error {
	return ackHandler(tag, multiple)
}

func (a *acknowledger) Nack(tag uint64, multiple bool, requeue bool) error {
	return nackHandler(tag, multiple, requeue)
}

func (a *acknowledger) Reject(tag uint64, requeue bool) error {
	return rejectHandler(tag, requeue)
}

// Helper to create a mock delivery message
func createDeliveryMessage(headers map[string]interface{}, body string, routingKey, correlationID string) *amqp.Delivery {
	return &amqp.Delivery{
		RoutingKey:    routingKey,
		CorrelationId: correlationID,
		Headers:       headers,
		Body:          []byte(body),
		Acknowledger:  &acknowledger{},
	}
}

func TestNewContextWithMessage(t *testing.T) {
	t.Run("Sets Session from Message", func(t *testing.T) {
		headers := map[string]interface{}{
			session.AmqpUserAgentHeader:    "Test-Agent",
			session.AmqpUserLanguageHeader: "en",
			session.AmqpSourceIpHeader:     "127.0.0.1",
		}
		message := createDeliveryMessage(headers, "Test Message", "test-key", "correlation-123")

		ctx := NewContextWithMessage(message)
		s := session.FromContext(ctx)

		// Assertions
		assert.Equal(t, "test-key", s.Uri)
		assert.Equal(t, "AMQP", s.Method)
		assert.Equal(t, "Test Message", s.Body)
		assert.Equal(t, "correlation-123", s.CorrelationId)
		assert.Equal(t, "Test-Agent", s.UserAgent)
		assert.Equal(t, "en", s.UserLanguage)
		assert.Equal(t, "127.0.0.1", s.SourceIp)
	})

	t.Run("Handles Missing Headers Gracefully", func(t *testing.T) {
		message := createDeliveryMessage(nil, "Test Body", "test-key", "correlation-123")
		ctx := NewContextWithMessage(message)
		s := session.FromContext(ctx)

		// Assertions
		assert.Equal(t, "test-key", s.Uri)
		assert.Equal(t, "", s.UserLanguage)
		assert.Equal(t, "", s.SourceIp)
	})
}

func TestBusConsumer(t *testing.T) {
	mockLogger, _ := pry.New("trace") // Mock logger

	t.Run("FailedStart", func(t *testing.T) {
		mockHandler := &mockHandler{
			keys: []string{"test.key"},
		}
		commands := map[string]Handler{
			"test.key": mockHandler,
		}
		mockTopic := &mockTopic{
			err: errors.New("mock error"),
		}
		consumer := NewBusConsumer(time.Second, mockLogger)
		err := consumer.Start(mockTopic, commands)
		assert.EqualError(t, err, "mock error")
	})

	t.Run("Ack", func(t *testing.T) {
		mockHandler := &mockHandler{
			keys: []string{"test.key"},
		}
		commands := map[string]Handler{
			"test.key": mockHandler,
		}
		mockTopic := &mockTopic{
			messages: []*amqp.Delivery{
				createDeliveryMessage(nil, "payload", "test.key", ""),
			},
		}

		consumer := NewBusConsumer(time.Second, mockLogger)
		var ackedMessage bool
		ackHandler = func(tag uint64, multiple bool) error {
			ackedMessage = true
			return nil
		}

		err := consumer.Start(mockTopic, commands)
		assert.Nil(t, err)
		assert.True(t, mockHandler.called)
		assert.True(t, ackedMessage)
	})

	t.Run("AckWildcard", func(t *testing.T) {
		mockHandler := &mockHandler{
			keys: []string{"*"},
		}
		commands := map[string]Handler{
			"*": mockHandler,
		}
		mockTopic := &mockTopic{
			messages: []*amqp.Delivery{
				createDeliveryMessage(nil, "payload", "unknown.routing.key", ""),
			},
		}

		consumer := NewBusConsumer(1*time.Second, mockLogger)
		var ackedMessage bool
		ackHandler = func(tag uint64, multiple bool) error {
			ackedMessage = true
			err := consumer.Close()
			assert.Nil(t, err)
			return nil
		}

		err := consumer.Start(mockTopic, commands)
		assert.Nil(t, err)
		assert.True(t, mockHandler.called)
		assert.True(t, ackedMessage)
	})

	t.Run("AckUnknown", func(t *testing.T) {
		mockTopic := &mockTopic{
			messages: []*amqp.Delivery{
				createDeliveryMessage(nil, "payload", "unhandled.key", ""),
			},
		}

		consumer := NewBusConsumer(1*time.Second, mockLogger)

		var ackedMessage bool
		ackHandler = func(tag uint64, multiple bool) error {
			ackedMessage = true
			return nil
		}

		err := consumer.Start(mockTopic, nil)
		assert.Nil(t, err)
		assert.True(t, ackedMessage, "Message without handler should be nacked")
	})

	t.Run("Nack", func(t *testing.T) {
		mockHandler := &mockHandler{
			keys:   []string{"test.key"},
			err:    errors.New("mock error"),
			called: false,
		}
		commands := map[string]Handler{
			"test.key": mockHandler,
		}
		mockTopic := &mockTopic{
			messages: []*amqp.Delivery{
				createDeliveryMessage(nil, "payload", "test.key", ""),
			},
		}

		consumer := NewBusConsumer(time.Hour, mockLogger)
		var nAckedMessage bool
		nackHandler = func(tag uint64, multiple bool, requeue bool) error {
			nAckedMessage = true
			err := consumer.Close()
			assert.Nil(t, err)
			return nil
		}

		err := consumer.Start(mockTopic, commands)
		assert.Nil(t, err)
		assert.True(t, mockHandler.called)
		assert.True(t, nAckedMessage)
	})

	t.Run("NackPanic", func(t *testing.T) {
		mockHandler := &mockHandler{
			keys:   []string{"test.key"},
			called: false,
			handler: func(ctx context.Context, payload []byte) error {
				panic("mock panic")
			},
		}
		commands := map[string]Handler{
			"test.key": mockHandler,
		}
		mockTopic := &mockTopic{
			messages: []*amqp.Delivery{
				createDeliveryMessage(nil, "payload", "test.key", ""),
			},
		}

		consumer := NewBusConsumer(time.Hour, mockLogger)
		var nAckedMessage bool
		nackHandler = func(tag uint64, multiple bool, requeue bool) error {
			nAckedMessage = true
			err := consumer.Close()
			assert.Nil(t, err)
			return nil
		}

		err := consumer.Start(mockTopic, commands)
		assert.Nil(t, err)
		assert.True(t, mockHandler.called)
		assert.True(t, nAckedMessage)
	})
}

// Mock Handler
type mockHandler struct {
	called  bool
	handler func(ctx context.Context, payload []byte) error
	err     error
	keys    []string
}

func (m *mockHandler) Run(ctx context.Context, payload []byte) error {
	m.called = true
	if m.err != nil {
		return m.err
	}
	if m.handler != nil {
		return m.handler(ctx, payload)
	}
	return nil
}

func (m *mockHandler) Keys() []string {
	return m.keys
}

// Mock Topic
type mockTopic struct {
	messages []*amqp.Delivery
	err      error
}

func (m *mockTopic) Start() (<-chan amqp.Delivery, error) {
	if m.err != nil {
		return nil, m.err
	}
	messages := make(chan amqp.Delivery)
	go func() {
		for _, msg := range m.messages {
			messages <- *msg
		}
		close(messages)
	}()
	return messages, nil
}

func (m *mockTopic) Stop() {
	// No-op for mock
}

func TestTopic(t *testing.T) {
	t.Run("Start with Invalid Config", func(t *testing.T) {
		topic := NewTopic("", "exchange", "queue", "routing-key")
		_, err := topic.Start()
		if err != ErrInvalidTopic {
			t.Errorf("Expected error %v, got %v", ErrInvalidTopic, err)
		}
	})
}
