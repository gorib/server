package rabbit

import (
	"fmt"

	amqp "github.com/rabbitmq/amqp091-go"
)

var ErrInvalidTopic = fmt.Errorf("invalid topic config")

type topic struct {
	dsn        string
	exchange   string
	queue      string
	routingKey string
	connection *amqp.Connection
	channel    *amqp.Channel
}

func NewTopic(dsn, exchange, queue, routingKey string) *topic {
	return &topic{
		dsn:        dsn,
		exchange:   exchange,
		queue:      queue,
		routingKey: routingKey,
	}
}

func (c *topic) Start() (_ <-chan amqp.Delivery, err error) {
	c.Stop()
	if c.dsn == "" || c.queue == "" || c.exchange == "" {
		return nil, ErrInvalidTopic
	}
	if c.routingKey == "" {
		c.routingKey = "#"
	}
	c.connection, err = amqp.Dial(c.dsn)
	if err != nil {
		return nil, fmt.Errorf("topic conn: %v", err)
	}
	c.channel, err = c.connection.Channel()
	if err != nil {
		return nil, fmt.Errorf("topic channel: %v", err)
	}
	queue, err := c.channel.QueueDeclare(c.queue, true, false, false, false, nil)
	if err != nil {
		return nil, fmt.Errorf("topic queue: %v", err)
	}
	err = c.channel.ExchangeDeclare(c.exchange, "topic", true, false, false, false, nil)
	if err != nil {
		return nil, fmt.Errorf("rabbit exchange: %v", err)
	}
	err = c.channel.QueueBind(queue.Name, c.routingKey, c.exchange, false, nil)
	if err != nil {
		return nil, fmt.Errorf("topic bind: %v", err)
	}

	return c.channel.Consume(queue.Name, "", false, false, false, false, nil)
}

func (c *topic) Stop() {
	if c.connection != nil {
		_ = c.connection.Close()
	}
	if c.channel != nil {
		_ = c.channel.Close()
	}
}
