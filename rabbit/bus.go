package rabbit

import (
	"context"
	"fmt"
	"time"

	amqp "github.com/rabbitmq/amqp091-go"

	"gitlab.com/gorib/pry"
	"gitlab.com/gorib/try"
)

const DefaultHandlerKey = "*"

type Handler interface {
	Run(ctx context.Context, payload []byte) error
	Keys() []string
}

type Topic interface {
	Start() (<-chan amqp.Delivery, error)
	Stop()
}

type busConsumer struct {
	failsafeTimeout time.Duration
	logger          pry.Logger
	stop            chan struct{}
}

func NewBusConsumer(failsafeTimeout time.Duration, logger pry.Logger) *busConsumer {
	return &busConsumer{
		failsafeTimeout: failsafeTimeout,
		logger:          logger,
		stop:            make(chan struct{}, 1),
	}
}

func (c *busConsumer) Start(connection Topic, commands map[string]Handler) error {
	messages, err := connection.Start()
	if err != nil {
		return err
	}
	defer connection.Stop()

	timer := time.NewTimer(c.failsafeTimeout)
	for {
		select {
		case <-c.stop:
			return nil
		case <-timer.C:
			return nil
		case message := <-messages:
			isDone := true
			command, ok := commands[message.RoutingKey]
			if !ok {
				command, ok = commands[DefaultHandlerKey]
			}
			if ok {
				ctx := NewContextWithMessage(&message)
				c.logger.Trace(fmt.Sprintf("%s%s", message.RoutingKey, string(message.Body)))
				try.Catch(func() {
					err = command.Run(ctx, message.Body)
				}, func(throwable error) {
					err = throwable
				})
				if err != nil {
					c.logger.Error(err, pry.Ctx(ctx))
					_ = message.Nack(false, true)
					time.Sleep(time.Second)
					isDone = false
				}
			}
			if isDone {
				_ = message.Ack(false)
			}
		}
	}
}

func (c *busConsumer) Close() error {
	c.stop <- struct{}{}
	return nil
}
