module gitlab.com/gorib/server

go 1.22.0

require (
	github.com/gofiber/fiber/v2 v2.52.6
	github.com/rabbitmq/amqp091-go v1.10.0
	github.com/stretchr/testify v1.10.0
	github.com/valyala/fasthttp v1.59.0
	gitlab.com/gorib/parser v0.0.0-20250224144300-e9d1752a4158
	gitlab.com/gorib/pry v0.0.0-20250224143323-d269032b7208
	gitlab.com/gorib/ptr v0.0.0-20250202074625-505b05714bce
	gitlab.com/gorib/session v0.0.0-20250224141908-df66ea5a990f
	gitlab.com/gorib/try v0.0.0-20250224141908-512acf4daa35
	google.golang.org/grpc v1.70.0
)

require (
	github.com/andybalholm/brotli v1.1.1 // indirect
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/google/uuid v1.6.0 // indirect
	github.com/klauspost/compress v1.18.0 // indirect
	github.com/mattn/go-colorable v0.1.14 // indirect
	github.com/mattn/go-isatty v0.0.20 // indirect
	github.com/mattn/go-runewidth v0.0.16 // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
	github.com/rivo/uniseg v0.4.7 // indirect
	github.com/rs/zerolog v1.33.0 // indirect
	github.com/valyala/bytebufferpool v1.0.0 // indirect
	golang.org/x/net v0.35.0 // indirect
	golang.org/x/sys v0.30.0 // indirect
	golang.org/x/text v0.22.0 // indirect
	google.golang.org/genproto/googleapis/rpc v0.0.0-20250219182151-9fdb1cabc7b2 // indirect
	google.golang.org/protobuf v1.36.5 // indirect
	gopkg.in/yaml.v3 v3.0.1 // indirect
)
