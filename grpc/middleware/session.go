package middleware

import (
	"context"
	"encoding/json"
	"strings"

	"google.golang.org/grpc"
	"google.golang.org/grpc/metadata"
	"google.golang.org/grpc/peer"

	"gitlab.com/gorib/session"
)

func Session(ctx context.Context, request any, info *grpc.UnaryServerInfo, handler grpc.UnaryHandler) (any, error) {
	var correlationId string
	var userAgent string
	var userLanguage string
	var sourceIp string
	if md, ok := metadata.FromIncomingContext(ctx); ok {
		if values := md.Get(session.GrpcCorrelationIdHeader); len(values) > 0 {
			correlationId = values[0]
		}
		if values := md.Get(session.GrpcUserAgent); len(values) > 0 {
			userAgent = values[0]
		}
		if values := md.Get(session.GrpcUserLanguage); len(values) > 0 {
			userLanguage = values[0]
		}
		if values := md.Get(session.GrpcSourceIp); len(values) > 0 {
			sourceIp = values[0]
		} else if peerValue, ok := peer.FromContext(ctx); ok {
			sourceIp = strings.Split(peerValue.Addr.String(), ":")[0]
		}
	}

	body, err := json.Marshal(request)
	if err != nil {
		return nil, err
	}
	ctx = session.NewContextWithSession(ctx, session.Session{
		Uri:           info.FullMethod,
		Method:        "GRPC",
		Body:          string(body),
		CorrelationId: correlationId,
		UserAgent:     userAgent,
		UserLanguage:  userLanguage,
		SourceIp:      sourceIp,
	})
	return handler(ctx, request)
}
