package middleware

import (
	"context"
	"fmt"

	"google.golang.org/grpc"
)

func Recover(ctx context.Context, request any, _ *grpc.UnaryServerInfo, handler grpc.UnaryHandler) (response any, err error) {
	defer func() {
		if r := recover(); r != nil {
			var ok bool
			if err, ok = r.(error); !ok {
				err = fmt.Errorf("%v", r)
			}
		}
	}()
	return handler(ctx, request)
}
