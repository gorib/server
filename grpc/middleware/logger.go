package middleware

import (
	"context"
	"fmt"
	"regexp"
	"slices"

	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"

	"gitlab.com/gorib/pry"
)

type Loggable interface {
	Log(ctx context.Context, request any, info *grpc.UnaryServerInfo, handler grpc.UnaryHandler) (any, error)
}

var allowedCodes = []codes.Code{
	codes.InvalidArgument,
	codes.NotFound,
	codes.AlreadyExists,
	codes.Unauthenticated,
	codes.PermissionDenied,
	codes.OutOfRange,
	codes.ResourceExhausted,
	codes.Aborted,
}

func Log(logger pry.Logger) func(ctx context.Context, request any, info *grpc.UnaryServerInfo, handler grpc.UnaryHandler) (any, error) {
	return func(ctx context.Context, request any, info *grpc.UnaryServerInfo, handler grpc.UnaryHandler) (any, error) {
		if loggable, ok := info.Server.(Loggable); ok {
			return loggable.Log(ctx, request, info, handler)
		}

		match := regexp.MustCompile(`.*\.(.*)`)
		method := match.FindStringSubmatch(info.FullMethod)[1]
		logger.Trace(fmt.Sprintf("%s >> %v", method, request))
		response, err := handler(ctx, request)
		if err != nil && !slices.Contains(allowedCodes, status.Code(err)) {
			logger.Error(fmt.Sprintf("%s << %v", method, response), pry.Err(err), pry.Ctx(ctx))
		} else {
			logger.Trace(fmt.Sprintf("%s << %v", method, response), pry.Err(err), pry.Ctx(ctx))
		}
		return response, err
	}
}
