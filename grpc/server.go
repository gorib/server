package grpc

import (
	"context"
	"fmt"
	"net"

	"google.golang.org/grpc"
	_ "google.golang.org/grpc/encoding/gzip"

	"gitlab.com/gorib/pry"
	"gitlab.com/gorib/server/grpc/middleware"
)

type Handler interface {
	Setup(server *grpc.Server)
}

func NewServer(handler Handler, mw []grpc.UnaryServerInterceptor, addr net.Addr, logger pry.Logger) *server {
	return &server{
		handler:     handler,
		middlewares: mw,
		addr:        addr,
		logger:      logger,
	}
}

type server struct {
	handler     Handler
	middlewares []grpc.UnaryServerInterceptor
	addr        net.Addr
	connection  *grpc.Server
	logger      pry.Logger
}

func (c *server) Serve(ctx context.Context) error {
	c.logger.Trace("Service has been started")

	middlewares := append([]grpc.UnaryServerInterceptor{middleware.Session, middleware.Log(c.logger), middleware.Recover}, c.middlewares...)
	c.connection = grpc.NewServer(grpc.ChainUnaryInterceptor(middlewares...))

	c.handler.Setup(c.connection)

	listener, err := (&net.ListenConfig{}).Listen(ctx, c.addr.Network(), c.addr.String())
	if err != nil {
		return fmt.Errorf("grpc serve: %v", err)
	}
	return c.connection.Serve(listener)
}

func (c *server) Close() error {
	if c.connection != nil {
		c.connection.GracefulStop()
	}
	return nil
}
