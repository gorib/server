package host

import "fmt"

var ErrInvalidHost = fmt.Errorf("invalid host")

func NewListener(network, host, port string) (*listener, error) {
	if network == "" || port == "" {
		return nil, ErrInvalidHost
	}
	return &listener{network, host, port}, nil
}

type listener struct {
	network, host, port string
}

func (t *listener) Network() string {
	return t.network
}

func (t *listener) String() string {
	return fmt.Sprintf("%s:%s", t.host, t.port)
}
