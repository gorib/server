package middleware

import (
	"github.com/gofiber/fiber/v2"

	"gitlab.com/gorib/session"
)

func Session(ctx *fiber.Ctx) error {
	s := session.Setup(session.Session{
		Uri:           ctx.Context().Request.URI().String(),
		Method:        string(ctx.Context().Request.Header.Method()),
		Body:          string(ctx.Context().Request.Body()),
		CorrelationId: string(ctx.Context().Request.Header.Peek(session.HttpCorrelationIdHeader)),
		UserAgent:     string(ctx.Context().Request.Header.Peek(session.HttpUserAgentHeader)),
		UserLanguage:  string(ctx.Context().Request.Header.Peek(session.HttpUserLanguageHeader)),
		SourceIp:      string(ctx.Context().Request.Header.Peek(session.HttpSourceIpHeader)),
	})
	ctx.Context().SetUserValue(session.Key{}, s)
	ctx.Response().Header.Set(session.HttpCorrelationIdHeader, s.CorrelationId)

	return ctx.Next()
}
