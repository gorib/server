package middleware

import (
	"fmt"
	"unsafe"

	"github.com/gofiber/fiber/v2"

	"gitlab.com/gorib/pry"
)

func Log(logger pry.Logger) func(ctx *fiber.Ctx) error {
	return func(ctx *fiber.Ctx) error {
		logger.Info(fmt.Sprintf("%s %s >> %s", ctx.Method(), ctx.Path(), string(ctx.Body())))
		err := ctx.Next()
		bytes, _ := ctx.Response().BodyUncompressed()
		body := *(*string)(unsafe.Pointer(&bytes))
		if len(body) > 1024*20 {
			body = body[:1024*20]
		}
		message := fmt.Sprintf("%s %s << %v", ctx.Method(), ctx.Path(), body)
		if ctx.Response().StatusCode() < 500 {
			logger.Trace(message, pry.Ctx(ctx.Context()), pry.Err(err))
		} else {
			logger.Error(message, pry.Ctx(ctx.Context()), pry.Err(err))
		}
		return err
	}
}
