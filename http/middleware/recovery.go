package middleware

import (
	"encoding/json"
	"errors"
	"fmt"
	"net/http"

	"github.com/gofiber/fiber/v2"
	"github.com/valyala/fasthttp"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"

	"gitlab.com/gorib/parser"
	"gitlab.com/gorib/server/http/model"
)

func Recover(ctx *fiber.Ctx) (err error) {
	defer func() {
		if r := recover(); r != nil {
			var ok bool
			if err, ok = r.(error); !ok {
				err = fmt.Errorf("%v", r)
			}
		}
		if err == nil {
			return
		}

		code := fasthttp.StatusInternalServerError
		errorMessage := err.Error()
		switch throw := err.(type) {
		case model.Error:
			code = throw.Code
		case *fiber.Error:
			code = throw.Code
		default:
			if errors.Is(err, parser.ErrCannotParse) {
				code = fasthttp.StatusBadRequest
			} else if statusErr, ok := status.FromError(err); ok {
				switch statusErr.Code() {
				case codes.InvalidArgument:
					code = http.StatusBadRequest
				case codes.NotFound:
					code = http.StatusNotFound
				case codes.AlreadyExists:
					code = http.StatusConflict
				case codes.PermissionDenied:
					code = http.StatusMethodNotAllowed
				case codes.OutOfRange:
					code = http.StatusUnprocessableEntity
				case codes.ResourceExhausted:
					code = http.StatusTooManyRequests
				case codes.DeadlineExceeded:
					code = http.StatusGone
				case codes.Unauthenticated:
					code = http.StatusUnauthorized
				}
				errorMessage = statusErr.Message()
			}
		}

		ctx.Response().SetStatusCode(code)
		ctx.Response().Header.Add("Content-Type", "application/json")

		message := parseMessage(errorMessage)
		if message["message"] == nil {
			message["message"] = errorMessage
		}
		if message["code"] == nil {
			message["code"] = code
		}

		var data []byte
		data, err = json.Marshal(message)
		if err != nil {
			err = fmt.Errorf("http recover: %v", err)
		}
		ctx.Response().SetBody(data)
		err = nil
	}()

	return ctx.Next()
}

func parseMessage(msg string) (message map[string]any) {
	message = map[string]any{}

	var payload map[string]any
	if err := json.Unmarshal([]byte(msg), &payload); err != nil {
		return
	}

	for key, value := range payload {
		switch key {
		case "message":
			var ok bool
			var errMsg string
			if errMsg, ok = value.(string); ok {
				message["message"] = errMsg
			}
		case "code":
			var ok bool
			var errCode float64
			if errCode, ok = value.(float64); ok {
				message["code"] = errCode
			}
		default:
			message[key] = value
		}
	}
	return
}
