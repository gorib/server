package middleware

import "github.com/gofiber/fiber/v2"

func Health(ctx *fiber.Ctx) error {
	if ctx.Path() == "/health" {
		return nil
	}
	return ctx.Next()
}
