package http

import (
	"context"
	"net"

	"github.com/gofiber/fiber/v2"
	"github.com/gofiber/fiber/v2/middleware/compress"

	"gitlab.com/gorib/pry"
	"gitlab.com/gorib/server/http/middleware"
)

type Router interface {
	Setup(app *fiber.App)
}

func NewServer(router Router, addr net.Addr, logger pry.Logger) *server {
	return &server{router: router, addr: addr, logger: logger}
}

type server struct {
	connection *fiber.App
	router     Router
	addr       net.Addr
	logger     pry.Logger
}

func (c *server) Serve(ctx context.Context) error {
	c.logger.Trace("Service has been started")

	c.connection = fiber.New(fiber.Config{DisableStartupMessage: true})
	c.connection.Use(compress.New(), middleware.Health, middleware.Session, middleware.Log(c.logger), middleware.Recover)

	c.router.Setup(c.connection)

	return c.connection.Listen(c.addr.String())
}

func (c *server) Close() error {
	if c.connection != nil {
		return c.connection.Shutdown()
	}
	return nil
}
