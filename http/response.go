package http

import (
	"encoding/json"
	"unsafe"

	"gitlab.com/gorib/ptr"
)

type Response interface {
	Content() string
	Type() string
	Code() uint
}

type response struct {
	contentType string
	content     *string
	code        uint
}

func (r *response) Content() string {
	return ptr.V(r.content)
}

func (r *response) Type() string {
	return r.contentType
}

func (r *response) Code() uint {
	return r.code
}

func NewResponse[T any](code uint, content *T) (Response, error) {
	return rawResponse(code, "text/plain", content)
}

func NewJsonResponse[T any](code uint, content *T) (Response, error) {
	return rawResponse(code, "application/json", content)
}

func rawResponse(code uint, contentType string, content any) (*response, error) {
	var data *string
	switch t := content.(type) {
	case *string:
		data = t
	case *[]byte:
		data = (*string)(unsafe.Pointer(t))
	default:
		b, err := json.Marshal(content)
		if err != nil {
			return nil, err
		}
		data = ptr.P(string(b))
	}
	return &response{
		code:        code,
		contentType: contentType,
		content:     data,
	}, nil
}
