package http

import (
	"github.com/gofiber/fiber/v2"
)

func Handler(handler func(ctx *fiber.Ctx) (Response, error)) func(ctx *fiber.Ctx) error {
	return func(ctx *fiber.Ctx) error {
		result, err := handler(ctx)
		if err != nil {
			return err
		}
		if result != nil {
			ctx.Response().Header.Add("Content-Type", result.Type())
			ctx.Response().SetStatusCode(int(result.Code()))
			ctx.Response().SetBodyString(result.Content())
		}

		return nil
	}
}
