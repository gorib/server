package http

import "gitlab.com/gorib/server/http/model"

func NewError(code int, message string) model.Error {
	return model.Error{Message: message, Code: code}
}
